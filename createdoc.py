#!/usr/bin/python
import os,sys,string
import xml.sax
import xml.sax.handler
import xml.sax.xmlreader
import zipfile
import sys,getopt
import os


# SAX handler to parse the Document.xml
class DocumentHandler(xml.sax.handler.ContentHandler):
 	def __init__(self, dirname):
 		self.files = []
 		self.dirname = dirname
 
 	def startElement(self, name, attributes):
 		item=attributes.get("file")
 		if item != None:
 			self.files.append(os.path.join(self.dirname,str(item)))
 
 	def characters(self, data):
 		return
 
 	def endElement(self, name):
 		return
 
def extractDocument(filename, outpath):
 	zfile=zipfile.ZipFile(filename)
 	files=zfile.namelist()
 	os.makedirs(os.path.dirname(outpath), exist_ok=True)
 	for i in files:
 		data=zfile.read(i)
 		dirs=i.split("/")
 		if len(dirs) > 1:
 			dirs.pop()
 			curpath=outpath
 			for j in dirs:
 				curpath=curpath+"/"+j
 				os.mkdir(curpath)
 		output=open(outpath+"/"+i,'wb')
 		output.write(data)
 		output.close()
 
def createDocument(filename, outpath):
        files=getFilesList(filename)
        dirname=os.path.dirname(filename)
        guixml=os.path.join(dirname,"GuiDocument.xml")
        if os.path.exists(guixml):
                print('GuiDocument.xml found in '+filename)
                files.extend(getFilesList(guixml))
        os.makedirs(os.path.dirname(outpath), exist_ok=True)
        compress=zipfile.ZipFile(outpath,'w',zipfile.ZIP_DEFLATED)
        for i in files:
                dirs=os.path.split(i)
                #print i, dirs[-1]
                compress.write(i,dirs[-1],zipfile.ZIP_DEFLATED)
        compress.close()
 
def getFilesList(filename):
       dirname=os.path.dirname(filename)
       handler=DocumentHandler(dirname)
       parser=xml.sax.make_parser()
       parser.setContentHandler(handler)
       parser.parse(filename)
 
       files=[]
       files.append(filename)
       files.extend(iter(handler.files))
       return files


inputfile = 'C:/dev/CAD/attach/Document.xml'
outputfile = 'C:/dev/CAD/proj/zaza/oproject.fcstd'
#print ('command', sys.argv[1:])
try:
        opts, args = getopt.getopt(sys.argv[1:],"h:i:o:",['in=','out='])
except getopt.GetoptError:
                print ('test.py -i <inputfile> -o <outputfile>')
             #sys.exit(2)
for opt, optvalue in opts:
       if opt == '-h':
                print ('createdoc.py -i <inputfile> -o <outputfile>')
                #sys.exit()
       elif opt in ("-i", "--in"):
                inputfile = optvalue
                #print('source folder = '+os.path.dirname(inputfile))
       elif opt in ("-o", "--out"):
                outputfile = optvalue
                if os.path.isfile(outputfile):                
                        
                        parts = outputfile.split('.')
                        print('file exists : ',parts)
                        newfile =''
                        length=len(parts)
                        for index in range(length):
                                if parts[index] == "":
                                        newfile+="."
                                if index == length-1:
                                        newfile+="_back."+parts[index]
                                else :
                                        newfile+=parts[index]
                        outputfile = newfile
                        print("new file :",newfile)
                #print('target file = '+outputfile)
       else :
                print('option : '+opt+' = val : '+optvalue)
print ('Input file is :', inputfile)
print ('Output file is :', outputfile)
   
#example:py createdoc.py -i C:/dev/CAD/newproject/Document.xml -o C:/dev/CAD/proj/newproject.fcstd
createDocument(inputfile, outputfile)
