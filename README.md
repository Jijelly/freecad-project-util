# Freecad Project Util

A simple python script to allow reassembling a freecad unzipped file into a compact readable cad project.

The goal of this project is to allow git versionning of freecad files
expanding the zip and generating back the required files will avoid to save binary files
to git and give an actual changelog to the freecad users.


* Replaces the Project Util command inside freecad
* Creates the required folders
* avoids replacing current file if exists by generating with _back suffix

